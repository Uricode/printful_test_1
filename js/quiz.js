class Quiz {

	constructor() {
		this.screen = 'first';
		this.load_tests();
	}

	async load_tests() {
		var response = await fetch(url, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify({'action':'load_tests'})});
		var tests = await response.json();
		this.fill_tests_select(tests);
	}

	fill_tests_select(tests) {
		var select = document.getElementById("test_input");
		for (let test of tests) {
			select.options[select.options.length] = new Option(test.name, test.id);
		}
	}

  async next() {
    var response = await fetch(url, {method: "POST", headers: {"Content-Type": "application/json"}, body: JSON.stringify(this.send_data)});
		var response_json = await response.json();
		if (response_json.question) {
			var question = response_json.question;
			this.send_data = {question_id:question.id, selected_answer:0};
			this.question = new Question(question, this);
			this.screen = 'question';
		} else if (response_json.result) {
			this.show_result(response_json.result);
		} else if (response_json.status == "error") {
			this.show_error(response_json.message);
		}
	}

	show_result(result) {
		document.getElementById('title_header').textContent = 'Thanks, ' + result.username + '!';
		var span = document.createElement('span');
		span.textContent = 'You responded correctly to ' + result.correct_answers + ' out of ' + result.question_count + ' questions';
		this.change_content(span);
		document.getElementById('primary-button').style.display = 'none';
	}

	show_error(message) {
		document.getElementById('title_header').textContent = 'There was an error';
		var span = document.createElement('span');
		span.textContent = message;
		this.change_content(span);
		document.getElementById('primary-button').style.display = 'none';
	}

	change_content(content) {
		var refillable = document.getElementById('refillable');
		refillable.innerHTML = '';
		refillable.appendChild(content);
	}
}

class Question {
	constructor(question, quiz) {
		this.quiz = quiz;
		this.display(question);
	}

	display(question) {
		var question_text = question.text;
		var answers = question.answers;
		var progress = question.progress;
		document.getElementById('title_header').textContent = question_text;
		let div = document.createElement('div');
		var buttons = this.buttons(answers);
		var progress = this.progress(progress);
		div.appendChild(buttons);
		div.appendChild(progress);
		this.quiz.change_content(div);
    document.getElementById('primary-button').value = "Next";
	}

	select_button () {
    var active = document.getElementsByClassName("active");
    if (active.length > 0) {
      active[0].className = active[0].className.replace(" active", "");
    }
    this.className += " active";
		var selected_answer = this.getAttribute('answer_id');
		pf_quiz.send_data.selected_answer = selected_answer;
 };

	progress(progress) {
		var position = parseInt(progress.position);
		var pending = parseInt(progress.pending);
		var total = position + pending;
		var percentage = 100 / total * position;
		var outer_progress = document.createElement('div');
		var inner_progress = document.createElement('div');

		outer_progress.setAttribute("class", 'row');
		outer_progress.setAttribute("id", 'outer_progress');
		inner_progress.setAttribute("id", 'inner_progress');
		inner_progress.style.width = percentage + '%';
		outer_progress.appendChild(inner_progress);
		return outer_progress;
	}

	buttons(answers) {
		var buttons = document.createElement('div');
		buttons.setAttribute("class", 'row');
		for (let answer of answers) {
			let button = document.createElement('button');
			button.setAttribute("answer_id", answer.id);
			button.setAttribute("class", "answer_button");
			button.textContent = answer.text;
			button.addEventListener('click', this.select_button, false);
			buttons.appendChild(button);
		}
		return buttons;
	}
}
