# Simple quiz test for Printful

## Set up
* Clone the project
* Place the back_end folder in a server running PHP
* Import the database dumps (back_end/db_dump) into a server running MySQL
* Enter the connection details in back_end/libs/config.ini
* Point the url variable in index.html (line 33) to the back-end
* You're ready to go!
