<?php

// Send headers according to your server configuration:

//header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST,GET,OPTIONS');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
header('Content-type: application/json; charset=utf-8');

require_once 'libs/database.php';
require_once 'model.php';
require_once 'quiz.php';

class Controller
{
    function __construct($quiz)
	{
		$this->quiz = $quiz;
        echo $this->process_input(file_get_contents("php://input"));
    }

    function process_input($input)
	{
        $this->received_data = $input;
        $posted_data = json_decode($input);
        if (property_exists($posted_data, 'username')) {
            // TODO Validate test_id is present
			$this->quiz->register_username($posted_data->username);
        }
        if (property_exists($posted_data, 'test_id')) {
			if (isset($posted_data->test_id)) {
                // TODO Validate test_id is an actual ID of a test
	            $output = $this->quiz->open_test($posted_data->test_id);
			} else {
				// Throw exception
			}
        } else if (property_exists($posted_data, 'action')) {
			$output = $this->quiz->all_tests();
        } else if (property_exists($posted_data, 'question_id')) {
            $question_id = $posted_data->question_id;
            $answer_id = $posted_data->selected_answer;
			if (!$this->quiz->check_answer($question_id, $answer_id)) {
                $output = '{"status":"error","message":"Your session has expired"}';
            } else {
                $output = $this->quiz->next_question($question_id);
            }
        } else {
			// Throw exception
		}
		return $output;
    }
}

$Db = new DB("local");
$model = new Model($Db);
$quiz = new Quiz($model);
$controller = new Controller($quiz);

