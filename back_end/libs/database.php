<?php
	class DB {
                
		function __construct($Server) 
		{
			$config = parse_ini_file('config.ini', true);
			$server = $config[$Server]['server'];
			$port = $config[$Server]['port'];
			$database = $config[$Server]['database'];
			$user = $config[$Server]['user'];
			$password = $config[$Server]['password'];
			$this->Connection = new PDO("mysql:host=$server;port=$port;dbname=$database", $user, $password, array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		}

		function Query ($query, $mode = PDO::FETCH_ASSOC) 
		{
			$sth = $this->Connection->prepare($query);
		    $sth->execute();
		    $ouput = $sth->fetchAll($mode);
			if (strtoupper(trim(substr($query, 0, 6))) == "SELECT") {
				return $ouput;
			} else if (strtoupper(trim(substr($query, 0, 6))) == "INSERT") {
				return $this->Connection->lastInsertId();
			}
		}
	}
