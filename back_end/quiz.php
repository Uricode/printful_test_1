<?php

class Quiz
{
    function __construct($model) {
		$this->model = $model;
		session_start();
        if (!isset($_SESSION['correct_answers'])) {
            $_SESSION['correct_answers'] = 0;
        }
    }

	public function register_username($username)
	{
		$_SESSION['username'] = $username;
	}

	public function all_tests()
	{
		return json_encode($this->model->getAllTests());
	}

	public function open_test($test_id)
	{
		$_SESSION['correct_answers'] = 0;
		$question_id = $this->model->getFirstQuestionId($test_id);
		return $this->open_question($question_id);
	}

	private function open_question($id)
	{
		$question = $this->model->getQuestion($id, true);

		$question['progress'] = $this->model->progress($id);
		$output = array(
		    'question' => $question,
		);
		return json_encode($output);
	}

	public function next_question($question_id)
	{
		$next_question_id = $this->model->getNextQuestionId($question_id);
		if ($next_question_id) {
			return $this->open_question($next_question_id);
		} else {
			/*	if there are no more questions we are going to serve the result,
				for which we are going to need the test ID and the total number
				of questions
			*/
			$test_id = $this->model->getTestId($question_id);
			$total_questions = $this->model->getQuestionCount($test_id);
			return $this->result($test_id, $total_questions);
		}
	}

	public function check_answer($question_id, $answer_id)
	{
		// If there is no session return false to the controller
        if (!isset($_SESSION['correct_answers'])) {
            return false;
        }

		$username = $_SESSION['username'];
		$this->model->save_answer($username, $question_id, $answer_id);

		if ($this->model->answer_is_correct($question_id, $answer_id)) {
            $_SESSION['correct_answers']++;
		}
        return true;
	}

	private function result($test_id, $question_count)
	{
		$correct_answers = $_SESSION['correct_answers'];
		$this->model->save_result($username, $test_id, $correct_answers);
		$result = array(
		    'result' => array(
		        'username' => $_SESSION['username'],
		        'correct_answers' => $correct_answers,
		        'question_count' => $question_count,
		    )
		);
		return json_encode($result);
	}
}
