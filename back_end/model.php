<?php
class Model
{
    /**
     * @param object $db Wrapper for a PDO object
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Could not connect to the database.');
        }
    }
    /**
     * Get all tests
     * @return object 
     */
    public function getAllTests()
    {
      $SQL = "SELECT * FROM test";
      return $this->db->query($SQL);
    }

    /**
     * Get first questions of a test
     */
    public function getFirstQuestionId($test_id)
    {
      $SQL = "SELECT id FROM question WHERE test_id = $test_id ORDER BY id LIMIT 1";
      return $this->db->query($SQL, PDO::FETCH_COLUMN)[0];
    }

    /**
     * Get a single question, optionally with its answers
     */
     public function getQuestion($id, $withAnswers = false)
     {
       $SQL = "SELECT * FROM question WHERE id = $id";
       $query = $this->db->query($SQL);
       //count($query) == 0 ? return false : $question = $query[0];
       if (count($query) > 0) {
         $question = $query[0];
       } else {
         return false;
       }
       if ($withAnswers) {
         $answers = $this->getAnswers($id);
         $question['answers'] = $answers;
       }
       return $question;
     }

    /**
     * Get answers for a question
     */
    public function getAnswers($question_id)
    {
      $SQL = "SELECT * FROM answer WHERE question_id = $question_id";
      return $this->db->query($SQL);
    }

    /**
     * Get next question id
     */
    public function getNextQuestionId($question_id)
    {
		$test_sql = "SELECT test_id FROM question WHERE id = $question_id;";
		$test_query = $this->db->query($test_sql, PDO::FETCH_COLUMN);
		$test_id = $test_query[0];
		$next_sql = "SELECT id, test_id FROM question WHERE test_id = $test_id AND id > $question_id ORDER BY id LIMIT 1;";
		$next_query = $this->db->query($next_sql);
		if (count($next_query) == 0) {
			return false;
		} else {
			return $next_query[0]['id'];
		}
    }

    /**
     * Determines if an answer is correct
     */
	public function answer_is_correct($question_id, $answer_id)
	{
		$answers = $this->db->query("SELECT correct FROM answer WHERE id = $answer_id AND question_id = $question_id;", PDO::FETCH_COLUMN);
		if (count($answers) == 0) {
		    return false;
		} else {
		    return $answers[0] == 1;
		}
	}

    /**
     * Get question count for a given test
     */
    public function getQuestionCount($test_id)
    {
		$SQL = "SELECT count(*) FROM question WHERE test_id = $test_id;";
		$query = $this->db->query($SQL, PDO::FETCH_COLUMN);
		return $query[0];
    }

    /**
     * Get test ID of a given question
     */
    public function getTestId($question_id)
    {
		$SQL = "SELECT test_id FROM question WHERE id = $question_id;";
		$query = $this->db->query($SQL, PDO::FETCH_COLUMN);
		return $query[0];
    }

    /**
     * Get test progress
     */
	public function progress ($question_id)
	{
		$test = $this->db->query("SELECT test_id FROM question WHERE id = $question_id;", PDO::FETCH_COLUMN);
		$test_id = $test[0];
		$position_questions = $this->db->query("SELECT count(*) FROM question WHERE test_id = $test_id AND id <= $question_id;", PDO::FETCH_COLUMN);
		$pending_questions = $this->db->query("SELECT count(*) FROM question WHERE test_id = $test_id AND id > $question_id;", PDO::FETCH_COLUMN);
		$position = $position_questions[0];
		$pending = $pending_questions[0];
		return array(
		    'position' => $position,
		    'pending' => $pending
		);
	}

	public function save_answer ($username, $question_id, $answer_id)
	{
		$SQL = "INSERT INTO reply (username, question_id, answer_id) VALUES ('$username', $question_id, $answer_id);";
		return $this->db->query($SQL);
	}

	public function save_result ($username, $test_id, $score)
	{
		$SQL = "INSERT INTO result (username, test_id, score) VALUES ('$username', $test_id, $score);";
		return $this->db->query($SQL);
	}
}
